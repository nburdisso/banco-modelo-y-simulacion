/* Modelo Banco  */


#include "simlib.c"
#include <stdio.h>
#include <stdlib.h> 


#define Arribo            1  /* Tipo de Evento 1: Arribos           */
#define Partida           2  /* Tipo de Evento 2: Partidas          */
#define AbreBanco         3  /* Tipo de Evento 3: Abre banco		*/
#define CierraBanco       4  /* Tipo de Evento 4: Cierra banco      */
#define Servidor_AC_1     1  /* Lista 1: Servidor Atencion al cliente */
#define Servidor_AC_2     2  /* Lista 2: Servidor Atencion al cliente */
#define Servidor_CA_1     3  /* Lista 3: Servidor Caja 1             */
#define Servidor_CA_2     4  /* Lista 3: Servidor Caja 2             */
#define Servidor_CA_3     5  /* Lista 3: Servidor Caja 3            */
#define Cola_1            6  /* Lista 4: Cola 1                     */
#define Cola_2            7  /* Lista 5: Cola 2                     */
#define Cola_3            8  /* Lista 5: Cola 3 (cola especial atencion al cliente 1)                    */
#define Cola_4            9  /* Lista 5: Cola 4  (cola especial atencion al cliente 1)                    */
#define Demora_cola_1     1  /* Sampst 1: Demora en Cola 1          */
#define Demora_cola_2     2  /* Sampst 2: Demora en Cola 2          */
#define Demora_cola_3     3  /* Sampst 2: Demora en Cola 2          */
#define Demora_cola_4     4  /* Sampst 2: Demora en Cola 2          */

/*transfer[1] --> sim_time*/
/*transfer[2] --> tipo evento*/
/*transfer[3] --> tipo de servidor*/  /*1,2 son AC y 3,4,5 son Caja*/
/*transfer[4] --> tipo de arribo */
/*transfer[5] --> id atencion al cliente */



/* Declaraci�n de variables propias */

float media_interarribos_ac_p, media_interarribos_ac_s, media_interarribos_c, media_servicio_ac_p, media_servicio_ac_s, media_servicio_c, media_servicio_cola_especial,media_servicio_fin_tramite;
int num_clientes, clientes_act, i;

//FILE *outfile, *stream2, *infile;



/* Declaraci�n de Funciones propias */

void inicializa(void);
void Rutina_arribos(void);
void Rutina_partidas(void);
void Rutina_abreBanco(void);
void Rutina_cierraBanco(void);
void Revisar_cajas(int id_arribo, int id_servidor, int esArribo);
void reporte(void);


int main()  /* Main function. */
{
	/* Apertura de Archivos que sean necesarios */
	float minimo, maximo;

	//indffile  = fopen("d:\entrada.txt", "r");
	//// Open for write 
	//  outfile = fopen( "d:\data2.txt", "w+" );
	//fprintf(outfile,"The file 'data2' was not opened\n" );

	//fscanf(infile, "%i %i", &minimo, &maximo);	

	media_interarribos_ac_p = 0.5;
	media_interarribos_ac_s = 1.2;
	media_interarribos_c = 0.058;
	media_servicio_ac_p = 0.5;
	media_servicio_ac_s = 0.75;
	media_servicio_c = 0.108;
	media_servicio_cola_especial = 0.0833333;
	num_clientes = 10000;

	/* Initializar Simlib */
	init_simlib();


	/* Establecer maxatr = M�ximo n�mero de Atributos utilizados  */
	maxatr = 4;

	/* Initializar el Sistema */
	inicializa();

	/* Ejecutar la simulaci�n. */

	clientes_act = 0;

	while ((clientes_act < num_clientes) && (sim_time < 120))
	{

		/* Determinar pr�ximo Evento */
		timing();

		/* Invoke the appropriate event function. */

		switch (next_event_type)
		{
		case Arribo:
			Rutina_arribos();
			break;
		case Partida:
			Rutina_partidas();
			break;
		case AbreBanco:
			Rutina_abreBanco();
			break;
		case CierraBanco:
			Rutina_cierraBanco();
			break;
		}
	}

	/* Llamada al Reporte para mostrar los resultados */
	reporte();
	//getchar();
	system("pause");
}

void inicializa(void)  /* Inicializar el Sistema */
{
	transfer[1] = sim_time + 8;
	transfer[2] = AbreBanco;
	list_file(INCREASING, LIST_EVENT);
}

void Rutina_abreBanco(void) {
	transfer[1] = sim_time + expon(media_interarribos_ac_p, Arribo);
	transfer[2] = Arribo;
	transfer[4] = 1; /*llegar por primera vez a atencion al cliente*/
	list_file(INCREASING, LIST_EVENT);

	transfer[1] = sim_time + expon(media_interarribos_ac_s, Arribo);
	transfer[2] = Arribo;
	transfer[4] = 2; /*llegar por segunda vez a atencion al cliente*/
	list_file(INCREASING, LIST_EVENT);

	transfer[1] = sim_time + expon(media_interarribos_c, Arribo);
	transfer[2] = Arribo;
	transfer[4] = 3; /*llegar a caja*/
	list_file(INCREASING, LIST_EVENT);

	transfer[1] = sim_time + 13;
	transfer[2] = CierraBanco;
	list_file(INCREASING, LIST_EVENT);

}

void Rutina_arribos(void)  /* Evento Arribo */	
{
	int tipo_arribo = transfer[4];
	/* Determinar pr�ximo arribo y cargar en Lista de Eventos */
	if (tipo_arribo == 1) {
		transfer[1] = sim_time + expon(media_interarribos_ac_p, Arribo);
		transfer[2] = Arribo;
		transfer[4] = 1;
		list_file(INCREASING, LIST_EVENT);
	}
	else if(tipo_arribo == 2)
	{
		transfer[1] = sim_time + expon(media_interarribos_ac_s, Arribo);
		transfer[2] = Arribo;
		transfer[4] = 2;
		list_file(INCREASING, LIST_EVENT);
	}
	else{
		transfer[1] = sim_time + expon(media_interarribos_c, Arribo);
		transfer[2] = Arribo;
		transfer[4] = 3; /*tipo de arribo a caja*/
		list_file(INCREASING, LIST_EVENT);
	}
	

	/* Chequear si el Servidor est� desocupado */
	if ((tipo_arribo== 1) || (tipo_arribo == 2)) {
		if (list_size[Servidor_AC_1] == 0)
		{
			++clientes_act;
			/* Si est� desocupado ocuparlo y generar la partida */

			list_file(FIRST, Servidor_AC_1);
			sampst(0.0, Demora_cola_1);
			if (tipo_arribo== 1) {
				transfer[1] = sim_time + expon(media_servicio_ac_p, Partida);
			}
			if (tipo_arribo == 2) {
				transfer[1] = sim_time + expon(media_servicio_ac_s, Partida);		
			}
			transfer[2] = Partida;
			/* Especifico de que servidor es la partida */
			transfer[3] = 1;
			transfer[4] = tipo_arribo;
			list_file(INCREASING, LIST_EVENT);
		}
		else if (list_size[Servidor_AC_2] == 0){
			++clientes_act;
			/* Si est� desocupado ocuparlo y generar la partida */
			list_file(FIRST, Servidor_AC_2);
			sampst(0.0, Demora_cola_1);
			if (tipo_arribo == 1) {
				transfer[1] = sim_time + expon(media_servicio_ac_p, Partida);
			}
			if (tipo_arribo == 2) {
				transfer[1] = sim_time + expon(media_servicio_ac_s, Partida);
			}
			transfer[2] = Partida;
			/* Especifico de que servidor es la partida */
			transfer[3] = 2;
			transfer[4] = tipo_arribo;
			list_file(INCREASING, LIST_EVENT);
		}
		else {
			transfer[1] = sim_time;
			transfer[2] = 0;
			transfer[3] = 0; //lo necesitamos?
			transfer[4] = tipo_arribo;
			transfer[5] = 0;
			list_file(LAST, Cola_1);
		}
		
	}
	else /*si es tipo arribo a caja*/
	{
		Revisar_cajas(tipo_arribo, 0, 1);
	}

}

void Rutina_partidas(void)  /* Evento Partida */ {
	/* Guardo de que servidor es la partida */

	int tipo_servidor = transfer[3];

	/* Guardo el tipo de arribo */
	
	int tipo_arribo = transfer[4];

	int id_ac = transfer[5]; /*es el id atencion al cliente del que sali�*/

	/* Desocupar el Servidor acorde al tipo de servidor que guarde */
	
	list_remove(FIRST, tipo_servidor);

	/* Ver de que servidor es la partida para usar la cola correspondiente al mismo */
	if (tipo_servidor <= 2) /*1,2 son AC y 3,4,5 son Caja*/
	{
		if (tipo_arribo == 2) {
			int prob = lcgrand(5);
			if (prob < 0.5) {
				
				Revisar_cajas(tipo_arribo, tipo_servidor, 1);
			}
		}
		if (tipo_servidor == 1) {
			if (list_size[Cola_3] > 0) /*pregunto si la cola especial de la atencion a cliente 1 tiene alguien esperando*/
			{
				/* Sacar el primero de la cola y actualizar Demoras */
				++clientes_act;
				list_remove(FIRST, Cola_3);
				sampst(sim_time - transfer[1], Demora_cola_3);

				list_file(FIRST, tipo_servidor);

				/* Cargo el tiempo de servicio segun el tipo de arribo*/
				transfer[1] = sim_time + media_servicio_cola_especial;
				transfer[2] = Partida;
				transfer[3] = tipo_servidor;
				transfer[4] = tipo_arribo;
				list_file(INCREASING, LIST_EVENT);
			}
			else if (list_size[Cola_1] > 0)
			{
				/* Sacar el primero de la cola y actualizar Demoras */

				++clientes_act;
				list_remove(FIRST, Cola_1);
				sampst(sim_time - transfer[1], Demora_cola_1);

				/* Cargar en el Servidor y generar la partida */
				list_file(FIRST, tipo_servidor);

				/* Cargo el tiempo de servicio fijo de atencion al cliente por segunda vez*/	
				if (tipo_arribo == 1) {
				transfer[1] = sim_time + expon(media_servicio_ac_p, Partida);
				}
				if (tipo_arribo == 2) {
					transfer[1] = sim_time + expon(media_servicio_ac_s, Partida);
				}
				transfer[2] = Partida;
				transfer[3] = tipo_servidor;
				transfer[4] = tipo_arribo;
				transfer[5] = 0;
				list_file(INCREASING, LIST_EVENT);

			}
			else{
				transfer[1] = sim_time;
				transfer[2] = 0;
				transfer[3] = 0;
				transfer[4] = tipo_arribo;
				transfer[5] = 0;
				list_file(LAST, Cola_1);
			}
		}
		else if (tipo_servidor == 2) {
			if (list_size[Cola_4] > 0)
			{
				/* Sacar el primero de la cola y actualizar Demoras */

				++clientes_act;
				list_remove(FIRST, Cola_4);
				sampst(sim_time - transfer[1], Demora_cola_4);

				/* Cargar en el Servidor y generar la partida */
				list_file(FIRST, tipo_servidor);

				/* Cargo el tiempo de servicio fijo de atencion al cliente por segunda vez*/
				transfer[1] = sim_time + media_servicio_cola_especial;
				transfer[2] = Partida;
				transfer[3] = tipo_servidor;
				transfer[4] = tipo_arribo;
				list_file(INCREASING, LIST_EVENT);
			}
			else if (list_size[Cola_1] > 0)
			{
				/* Sacar el primero de la cola y actualizar Demoras */

				++clientes_act;
				list_remove(FIRST, Cola_1);
				sampst(sim_time - transfer[1], Demora_cola_1);

				/* Cargar en el Servidor y generar la partida */
				list_file(FIRST, tipo_servidor);

				/* Cargo el tiempo de servicio fijo de atencion al cliente por segunda vez*/	
				if (tipo_arribo == 1) {
				transfer[1] = sim_time + expon(media_servicio_ac_p, Partida);
				}
				if (tipo_arribo == 2) {
					transfer[1] = sim_time + expon(media_servicio_ac_s, Partida);
				}
				transfer[2] = Partida;
				transfer[3] = tipo_servidor;
				transfer[4] = tipo_arribo;
				transfer[5] = 0;
				list_file(INCREASING, LIST_EVENT);

			}
			else{
				transfer[1] = sim_time;
				transfer[2] = 0;
				transfer[3] = 0;
				transfer[4] = tipo_arribo;
				transfer[5] = 0;
				list_file(LAST, Cola_1);
			}

		}
	}
	else
	{
		if (list_size[Cola_2] > 0){
			++clientes_act;

			/* Si est� desocupado ocuparlo y generar la partida */
			
				
			Revisar_cajas(tipo_arribo, id_ac, 0);
/*
			sampst(sim_time - transfer[1], Demora_cola_2);

			transfer[1] = sim_time + expon(media_servicio_c, Partida);
			transfer[2] = Partida;
			 Especifico de que servidor es la partida 
			transfer[3] = tipo_servidor;
			transfer[4] = tipo_arribo;*/
			/* Guardo que cola de atencion al cliente me atendio*/
			/* id_ac = 0 si no tiene que volver a la atencion al cliente*/
			/* id_ac > 0 si tiene que volver a la atencion al cliente*/
/*			transfer[5] = id_ac;
			list_file(INCREASING, LIST_EVENT);*/
		}
	
		if(id_ac == 1){
			transfer[1] = sim_time;
			transfer[2] = 0;
			transfer[3] = tipo_servidor;
			transfer[4] = tipo_arribo;
			transfer[5] = id_ac;
			list_file(LAST, Cola_3);
		}

		if(id_ac == 2){
			transfer[1] = sim_time;
			transfer[2] = 0;
			transfer[3] = tipo_servidor;
			transfer[4] = tipo_arribo;
			transfer[5] = id_ac;
			list_file(LAST, Cola_4);
		}

	}
}

void Rutina_cierraBanco(void) {
	transfer[1] = sim_time + 19;
	transfer[2] = AbreBanco;
	list_file(INCREASING, LIST_EVENT);

	event_cancel(Arribo); /*anula todos los arribos porque cierra el banco*/
}

void Revisar_cajas(int id_arribo, int id_servidor, int esArribo) {

	if (list_size[Servidor_CA_1] == 0)
	{
		++clientes_act;
		if (esArribo == 1){
			sampst(0.0, Demora_cola_2);
		}
		else {
			list_remove(FIRST, Cola_2);
			sampst(sim_time - transfer[1], Demora_cola_2);
		}



		/* Si est� desocupado ocuparlo y generar la partida */
		list_file(FIRST, Servidor_CA_1);		
		

		transfer[1] = sim_time + expon(media_servicio_c, Partida);
		transfer[2] = Partida;
		/* Especifico de que servidor es la partida */
		transfer[3] = 3; /*a que caja se dirige*/
		transfer[4] = id_arribo;
		/* Guardo que cola de atencion al cliente me atendio*/
		transfer[5] = id_servidor;
		list_file(INCREASING, LIST_EVENT);
	}
	else if (list_size[Servidor_CA_2] == 0) {
		++clientes_act;
		if (esArribo == 1){
			sampst(0.0, Demora_cola_2);
		}
		else {
			list_remove(FIRST, Cola_2);
			sampst(sim_time - transfer[1], Demora_cola_2);
		}
		/* Si est� desocupado ocuparlo y generar la partida */

		list_file(FIRST, Servidor_CA_2);

		transfer[1] = sim_time + expon(media_servicio_c, Partida);
		transfer[2] = Partida;
		/* Especifico de que servidor es la partida */
		transfer[3] = 4;  /*a que caja se dirige*/
		transfer[4] = id_arribo;
		/* Guardo que cola de atencion al cliente me atendio*/
		transfer[5] = id_servidor;
		list_file(INCREASING, LIST_EVENT);
	}
	else if (list_size[Servidor_CA_3] == 0){
		++clientes_act;
		if (esArribo == 1){
			sampst(0.0, Demora_cola_2);
		}
		else {
			list_remove(FIRST, Cola_2);
			sampst(sim_time - transfer[1], Demora_cola_2);
		}
		/* Si est� desocupado ocuparlo y generar la partida */

		list_file(FIRST, Servidor_CA_3);

		transfer[1] = sim_time + expon(media_servicio_c, Partida);
		transfer[2] = Partida;
		/* Especifico de que servidor es la partida */
		transfer[3] = 5;  /*a que caja se dirige*/
		transfer[4] = id_arribo;
		/* Guardo que cola de atencion al cliente me atendio*/
		transfer[5] = id_servidor;
		list_file(INCREASING, LIST_EVENT);
	}
	else {
		
		transfer[1] = sim_time;
		transfer[2] = 0;
		transfer[3] = 0;
		transfer[4] = id_arribo;
		transfer[5] = id_servidor;
		list_file(LAST, Cola_2);
	}


}


void reporte(void)  /* Generar Reporte de la Simulaci�n */
{

	/* Mostrar Par�metros de Entrada */

	/* -------- Por pantalla -------- */

	printf("Sistema M/M/1 - Simulaci�n con Simlib \n\n");
	printf("Media de Interarrbios la primera vez: %8.3f minutos\n", media_interarribos_ac_p);
	printf("Media de Interarrbios la segunda vez: %8.3f minutos\n", media_interarribos_ac_s);
	printf("Media de Servicios             : %8.3f minutos\n", media_servicio_c);
	printf("Cantidad de Clientes Demorados : %i \n\n", num_clientes);

	/* Calcular los Estad�sticos */

	/*ATENCION AL CLIENTE*/
	/* Estad�sticos Escalaras - Sampst */
	sampst(0.0, -Demora_cola_1);
	printf("\nDemora en Cola 1 (Atencion al cliente)                : %f \n ", transfer[1]);
	/* Estad�sticos Temporales - Timest y Filest */
	filest(Cola_1);
	printf("\nN�mero Promedio en Cola        : %f \n ", transfer[1]);
	filest(Servidor_AC_1);
	printf("\nUtilizaci�n Servidor 1         : %f \n ", transfer[1]);
	filest(Servidor_AC_2);
	printf("\nUtilizaci�n Servidor 2         : %f \n ", transfer[1]);


	

	/*CAJAS*/
	sampst(0.0, -Demora_cola_2);
	printf("\nDemora en Cola 2 (CAJAS)               : %f \n ", transfer[1]);
	filest(Cola_2);
	printf("\nN�mero Promedio en Cola 2      : %f \n ", transfer[1]);
	filest(Servidor_CA_1);
	printf("\nUtilizaci�n Servidor 1         : %f \n ", transfer[1]);
	filest(Servidor_CA_2);
	printf("\nUtilizaci�n Servidor 2         : %f \n ", transfer[1]);
	filest(Servidor_CA_3);
	printf("\nUtilizaci�n Servidor 3         : %f \n ", transfer[1]);
	printf("\nTiempo Final de Simulation     : %f horas\n", sim_time);
	

/*Espera listas especiles*/
	sampst(0.0, -Demora_cola_3);
	printf("\nDemora en Cola 3 (COLA ESPECIAL AT AL CLI 1)               : %f \n ", transfer[1]);
	filest(Cola_3);
	printf("\nN�mero Promedio en Cola 3      : %f \n ", transfer[1]);
	sampst(0.0, -Demora_cola_4);
	printf("\nDemora en Cola 4 (COLA ESPECIAL AT AL CLI 2)               : %f \n ", transfer[1]);
	filest(Cola_4);
	printf("\nN�mero Promedio en Cola 4      : %f \n ", transfer[1]);
	
	/* -------- En un archivo -------- */

	//fprintf( outfile, "Sistema M/M/1 - Simulaci�n con Simlib \n\n" );
	//fprintf( outfile, "Media de Interarribos          : %8.3f minutos\n", media_interarribos);
	//fprintf( outfile, "Media de Servicios             : %8.3f minutos\n", media_servicio);
	//fprintf( outfile, "Cantidad de Clientes Demorados : %i \n\n", num_clientes);

	/* Calcular los Estad�sticos */

	/* Estad�sticos Escalaras - Sampst */
	/*sampst(0.0, -Demora_cola_1);*/
	//fprintf( outfile, "\nDemora en Cola                 : %f \n ",transfer[1]);


	///* Estad�sticos Temporales - Timest y Filest */

	//filest(Cola);
	//fprintf( outfile, "\nN�mero Promedio en Cola        : %f \n ",transfer[1]);
	//filest(Servidor);
	//fprintf( outfile, "\nUtilizaci�n Servidor           : %f \n ",transfer[1]);
	//fprintf( outfile, "\nSimulation end time            : %10.3f minutes\n", sim_time );
	//fclose(outfile);

}

/* Demora media de los clientes de cr�ditos en cola de cajas
 * N�mero medio de clientes en la cola de cajasNumero medio de clientes que vienen por primer vez en la cola de espera del area de cr�ditos
 * Utilizaci�n Promedio de los representantes de atencion al cliente 
 * Cantidad de cr�ditos otorgdos durante la simulacion
 * Numero medio de clientes en el banco
 * */
